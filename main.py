import os, sys
# import alsaaudio
import pygame
#from pygame.locals import *

# from title_bar import TitleBar

def wrap(a, min, max):
	if a > max:
		ret = min
	elif a < min:
		ret = max
	else:	
		ret = a

	return ret

def bound(a, min, max):
	if a > max:
		ret = max
	elif a < min:
		ret = min
	else:
		ret = a

	return ret

def ChangeBrightness(newBrightness):
	BackLight = "/proc/driver/backlight"
	try:
		f = open(BackLight,'w')
	except IOError:
		print("Open write %s failed %d" % (BackLight,newBrightness))
		return False
	else:
		with f:
			f.write(str(newBrightness))
			return True


def ChangeVolume(newVolume): ##value 0-10
	newVolume = bound(newVolume, 0, 10)
	newVolume = newVolume * 10
	
	m = alsaaudio.Mixer()
	m.setvolume(int(newVolume))
	
def main():
	def refresh():
		background.fill(backGroundColour)

		for item in items:
			item.draw(background)
		
		pygame.draw.rect(background, shadeColour, (0, 0, 320, 25))
		background.blit(font.render("GameShell", 1, normalColour), (0, 6))

		pygame.draw.rect(background, normalColour, (-1, 240-20, 322, 21), 1)
		
		screen.blit(background, (0, 0))
		pygame.display.flip()

	def select():
		items[selectedItem].action(items[selectedItem])

	def cancel():
		pass

	#relativeLocation is the number of spaces to move dow (negative is up)
	def moveCursor(relativeLocation, select):
		items[select].colour = normalColour
		items[select].draw(background)

		select = wrap(select + relativeLocation, 0, len(items)-1)

		items[select].colour = highlightColour
		items[select].draw(background)

		refresh()

		return select

	def changeSlider(relativeAmount):
		items[selectedItem].value = bound(items[selectedItem].value + relativeAmount, 0, 10)
		items[selectedItem].action(items[selectedItem])

		items[selectedItem].draw(background)
		refresh()

	pygame.init()
	pygame.display.set_caption("menu")

	backGroundColour = (255, 255, 255)
	highlightColour = (25, 190, 239)
	shadeColour = (228, 228, 228)
	normalColour = (120, 120, 120)

	screen = pygame.display.set_mode((320, 240))
	background = pygame.Surface(screen.get_size())
	background = background.convert()
	background.fill(backGroundColour)
	
	font = pygame.font.Font(None, 24)

	items = []

	resumeButton = Button(10, 30, 240, 35, normalColour, "Resume", backGroundColour, font, background)
	items.append(resumeButton)
	quitButton = Button(10, 80, 240, 35, highlightColour, "Quit Game", backGroundColour, font, background)
	def quitButtonAction(self):
		print(self.text)
		print("quit")
	quitButton.action = quitButtonAction
	items.append(quitButton)
	volumeSlider = Slider(10, 130, 280, 30, normalColour, "Vol", backGroundColour, font, background)
	def volumeSliderAction(self):
		print("Volume: ", self.value)
		ChangeVolume(self.value)
	volumeSlider.action = volumeSliderAction
	items.append(volumeSlider)
	brightnessSlider = Slider(10, 180, 280, 30, normalColour, "Vol", backGroundColour, font, background)
	def brightnessSliderAction(self):
		print("Brightness: ", self.value)
		ChangeBrightness(self.value)
	brightnessSlider.action = brightnessSliderAction
	items.append(brightnessSlider)

	selectedItem = 1

	# quitButton.draw(background)
	# resumeButton.draw(background)
	# volumeSlider.draw(background)
	# brightnessSlider.draw(background)

	# TitleBar.Draw()

	refresh()
	running = True

	while running:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_UP:
					selectedItem = moveCursor(-1, selectedItem)
				if event.key == pygame.K_DOWN:
					selectedItem = moveCursor(1, selectedItem)
				if event.key == pygame.K_LEFT:
					changeSlider(-1)
				if event.key == pygame.K_RIGHT:
					changeSlider(1)
				# if event.key == pygame.K_J: #East button
				# 	select()
				# 	break
				# if event.key == pygame.K_K: #South button
				# 	cancel()
				# 	break
				# if event.key == pygame.K_I: #North button
				# 	break
				# if event.key == pygame.K_U: #West button
				# 	break
				if event.key == pygame.K_ESCAPE: #Menu button
					cancel()
					break
				if event.key == pygame.K_RETURN: #Start button
					break
				if event.key == pygame.K_SPACE: #Select button
					select()
					break

class Button:
	def __init__(self, x, y, width, height, colour, text, textColour, font, surface):
		#self.x = x
		self.y = y
		self.width = width
		self.height = height
		self.colour = colour
		self.text = text
		self.textColour = textColour
		self.font = font

		#self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
		self.rect = pygame.Rect((0, 0), (self.width, self.height))
		self.rect.center = (160, self.y+self.height/2)
		self.textSurface = font.render(self.text, 1, self.textColour)

	def action(self):
		pass

	def draw(self, surface):
		#pygame.draw.rect(surface, self.colour, surface.get_rect(centerx=(self.x+self.width/2)))	
		pygame.draw.rect(surface, self.colour, self.rect)
		#surface.blit(self.textSurface, self.textSurface.get_rect(center=(self.x+self.width/2, self.y+self.height/2)))
		surface.blit(self.textSurface, self.textSurface.get_rect(center=self.rect.center))

	def action(self, _self):
		print("button action: " + self.text)

class Slider:
	def __init__(self, x, y, width, height, colour, text, textColour, font, surface):
		#self.x = x
		self.y = y
		self.width = width
		self.height = height
		self.colour = colour
		self.text = text
		self.textColour = textColour
		self.font = font
		self.itemSpace = self.width/23
		self.itemWidth = self.width/23
		self.value = 5

		#self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
		self.rect = pygame.Rect((0, 0), (self.width, self.height))
		self.rect.center = (160, self.y+self.height/2)
		self.textSurface = font.render(self.text, 1, self.textColour)

	def draw(self, surface):
		pygame.draw.polygon(surface, self.colour, [(self.rect.left, self.rect.centery), (self.rect.left + self.itemWidth, self.rect.top), (self.rect.left + self.itemWidth, self.rect.bottom)])

		for marker in range(self.value):
			currentx = self.rect.left
			currentx = currentx + marker * self.itemSpace + self.itemSpace
			currentx = currentx + (marker + 1) * self.itemWidth

			if True: #eventually if volume is higher than this level, draw square, else draw dot
				pygame.draw.rect(surface, self.colour, (currentx, self.y, self.itemWidth, self.height))
			else:
				pass

		pygame.draw.polygon(surface, self.colour, [(self.rect.right, self.rect.centery), (self.rect.right - self.itemWidth, self.rect.top), (self.rect.right - self.itemWidth, self.rect.bottom)])

	def action(self, _self):
		print("slider action: " + self.text)
		

if __name__ == "__main__":
	main()
